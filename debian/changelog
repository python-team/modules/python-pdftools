python-pdftools (0.37-6) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:06:25 -0500

python-pdftools (0.37-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * Convert git repository from git-dpm to gbp layout

  [ Sandro Tosi ]
  * debian/control
    - switch me to maintainer, team to uploaders
    - bump Standards-Version to 4.3.0 (no changes needed)
  * debian/copyright
    - extend packaging copyright years
    - convert to machine-parsable format
  * bump compat level to 11

 -- Sandro Tosi <morph@debian.org>  Sat, 02 Feb 2019 00:14:54 -0500

python-pdftools (0.37-4) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Andrey Rahmatullin ]
  * Convert from dh_pysupport to dh_python2 (Closes: #786225)
    - add dh-python to Build-Depends
    - drop python-support from Build-Depends-Indep

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Thu, 20 Aug 2015 00:20:20 +0500

python-pdftools (0.37-3) unstable; urgency=low

  * debian/source/format
    - move to '3.0 (quilt)' source format
  * debian/patches/10_remove_version_check.patch
    - remove bogus version check; thanks to Jakub Wilk for the report;
      Closes: #606535
  * debian/control
    - bump Standards-Version to 3.9.1 (no changes needed)
  * debian/{control, rules}
    - switch to dh7 and short rules file
  * debian/docs
    - install README.txt

 -- Sandro Tosi <morph@debian.org>  Sun, 12 Dec 2010 11:19:52 +0100

python-pdftools (0.37-2) unstable; urgency=low

  * debian/control
    - switch Vcs-Browser field to viewsvn
    - updated my email address
    - bump Standards-Version to 3.8.0 (no changes needed)
    - removed XS-DM-Upload-Allowed field
    - build-depends only on python
  * debian/rules
    - only need to install and clean for the current python interpreter
    - merged 'rm' calls into 'dh_clean' one
  * debian/copyright
    - updated my email address
    - extended packaging copyright years
    - fix packaging copyright notice
    - link both upstream and packaging licenses to GPLv2

 -- Sandro Tosi <morph@debian.org>  Fri, 06 Feb 2009 11:05:46 +0100

python-pdftools (0.37-1) unstable; urgency=low

  [ Sandro Tosi ]
  * New upstream release

  [ Piotr Ożarowski ]
  * debian/control
    - added "XS-DM-Upload-Allowed: yes"

 -- Sandro Tosi <matrixhasu@gmail.com>  Mon, 28 Apr 2008 23:12:22 +0200

python-pdftools (0.36-1) unstable; urgency=low

  * New upstream release (Closes: #469500)
  * debian/control
    - given to DPMT
    - removed dpatch build-dep
    - bump Standard-Version to 3.7.3
    - build-dep on python-all and not python-all-dev
    - switched from python-central to python-support
  * debian/rules
    - removed dpatch stuff
    - switched from python-central to python-support
    - added README.txt installation
  * debian/copyright
    - extended copyright date
    - added local URI to GPL license text

 -- Sandro Tosi <matrixhasu@gmail.com>  Tue, 18 Mar 2008 18:02:14 +0100

python-pdftools (0.35-1) unstable; urgency=low

  * New upstream release
  * debian/patches/10_remove_shebangs.dpatch
    - removed since upstream merged it

 -- Sandro Tosi <matrixhasu@gmail.com>  Mon, 19 Nov 2007 23:30:50 +0100

python-pdftools (0.34.dfsg-1) unstable; urgency=low

  * Initial release (Closes: #440722)
    - LICENSE file added to the upstream sources tarball

 -- Sandro Tosi <matrixhasu@gmail.com>  Sun, 04 Nov 2007 00:13:24 +0100
